Yii2 FileHelper
===============
For individual use.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist platx/yii2-file-helper "*"
```

or add

```
"platx/yii2-file-helper": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Information for use in `\platx\filehelper\FileHelper` class.